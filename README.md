# tacon: Azserve framework

## Project structure

```mermaid
graph TD;
  bean-->annotation;
  bean-apt-->bean;
  jee-query-->annotation;
  italia-->annotation;
  enums-->annotation;
  common-->annotation;
  
  database-->annotation;
  jasper6-->annotation
  poi4-->annotation
  structure-->annotation
  i18n-annotation==>i18n
  i18n-apt-->i18n-annotation
  i18n-apt==>i18n

  pour

  subgraph frontend
  presenter;
  vaadin-flow-->presenter;
  end
  
  vaadin-flow-->bean;

  subgraph backend
  jee-validation;
  jee-entity;
  dto;
  jee-interface-legacy;
  jee-ejb-->jee-query;
  jee-ejb-->jee-entity;
  jee-ejb-->dto;
  jee-ejb-legacy-->jee-interface-legacy;
  jee-ejb-legacy-->jee-query;
  jee-ejb-legacy-->jee-entity;
  jee-ejb-legacy-->dto;
  end

```
* com.azserve.azframework.common.network.NetworkUtils -> common
